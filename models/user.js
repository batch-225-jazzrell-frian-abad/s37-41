
// ACTIVITY 1 : s37

// Create the Scheme, model and export the file
const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "email is required"]
	},
	password: {
		type: String,
		required: [true, "password is required"]
	},
	isAdmin : {
		type: Boolean,
		default : false
	},
	mobileNo : {
		type: String,
		required: [true, "Mobile No. is required"]
	},
	enrollments: [
		{
			courseId : {
				type: String,
				required: [true, "Course ID is required"]
			},
			enrolledOn : {
				type: Date,
				default: new Date()
			},
			status : {
				type: String,
				default: "Enrolled"
			}
		}



	]


});

// "module.exports" is the way for Node JS to treat this value as "package" that can be use to other files.


module.exports = mongoose.model('User', userSchema);